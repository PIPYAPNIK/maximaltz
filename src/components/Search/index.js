import React, { useState } from 'react';

function Search({ setPerson }) {

    const [ searchValue, setSearchValue ] = useState('');
    const [ searchFail, setSearchFail ] = useState(false);

    async function peopleRequest(personId)  {
        const personUrl = `https://swapi.dev/api/people/${personId}`;
        let personResponse = await fetch(personUrl);
    
        if (personResponse.ok) {
            let jsonPerson = await personResponse.json();
            const planetUrl = jsonPerson.homeworld;
            let planetResponse = await fetch(planetUrl);

            if (planetResponse.ok) {
                let jsonPlanet = await planetResponse.json();
                let result = {
                    name: jsonPerson.name,
                    height: jsonPerson.height,
                    weight: jsonPerson.mass,
                    eyeColor: jsonPerson.eye_color,
                    planet: jsonPlanet.name,
                    gender: jsonPerson.gender
                };
                setPerson(result);
            }
        } else {
            setPerson(personResponse.status);
        }
    };

    const handleInputChange = (e) => {
        const isInteger = /^[0-9]+$/;
        if (e.target.value === '' || isInteger.test(e.target.value)) {
            setSearchValue(e.target.value);
        }
    };

    const handleFormSubmit = (e) => {
        e.preventDefault();
        if(searchValue > 0 && searchValue <= 83) {
            peopleRequest(searchValue);
            setSearchFail(false);
        } else setSearchFail(true);
    };

    return (
        <div className="search">
            <h6 className="description">Введите число и получите персонажа</h6>
            {searchFail ? (<p className="error">Введите в поле чилсо от 1 до 83 включительно!</p>) : (<></>)}
            <form
                onSubmit={handleFormSubmit}
            >
            <input
                type="text"
                value={searchValue}
                onChange={handleInputChange}
            />
            <button>Найти</button>
            </form>
        </div>
    );
}

export default Search;