import React from 'react';

function Person({ name, planet, eyeColor, weight, height, gender }) {

  const indicatorsStyle = {
    backgroundColor: eyeColor,
    height: `${height}px`,
    width: `${weight}px`,
    borderRadius: gender === 'female' ? '15px' : '0px'
  };

  return (
      <div className="person">
          <div className="person__info">
                <div className="row">
                    <p><label>Имя: </label>{name}</p>
                </div>
                <div className="row">
                    <p><label>Планета: </label>{planet}</p>
                </div>
          </div>
          <div className="person__indicators" style={indicatorsStyle}></div>
      </div>
  );
}

export default Person;