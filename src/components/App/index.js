import React, {useState} from 'react';
import Header from '../Header';
import Search from '../Search';
import Person from '../Person';

function App() {

    const [ person, setPerson ] = useState(null);
    console.log(person);

    return (
    <div className='app'>
        <Header title="Персонажи StarWors"></Header>
        <Search setPerson={setPerson}/>
        {person !== null ? (
            <Person name={person.name} 
                    planet="Татуин" 
                    weight={person.weight} 
                    height={person.height} 
                    eyeColor={person.eyeColor} 
                    gender={person.gender} />
        ) : (<div className='person-empty'></div>)}
        <div className='created'>Created by PIPYAPNIK.</div>
    </div>
    );
}

export default App;